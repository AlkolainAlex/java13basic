package firstWeekPractice;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double s = scanner.nextInt();
        double d = Math.sqrt(s * 4 / Math.PI);
        double l = Math.PI * d;

        System.out.printf("args d = %s  l = %s", d, l);

    }
}