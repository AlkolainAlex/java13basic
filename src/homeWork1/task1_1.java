package homeWork1;

import java.util.Scanner;

public class task1_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double r = scanner.nextDouble();
        double v = 4 / 3.0 * Math.PI * Math.pow(r, 3);
        System.out.printf("Объем шара с радиусом %s равен = %s", r, v);
    }
}
