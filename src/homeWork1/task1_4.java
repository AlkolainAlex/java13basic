package homeWork1;

import java.util.Scanner;

public class task1_4 {
    public static void main(String[] args) {
        final int SECOND_PER_MINUTE = 60,
                MINUTES_PER_HOUR = 60;

        Scanner input = new Scanner(System.in);
        int count = input.nextInt();
        int totalMinute = count / SECOND_PER_MINUTE;
        int minute = totalMinute % SECOND_PER_MINUTE;
        int hour = totalMinute / MINUTES_PER_HOUR;
        System.out.println("Текущее время " + hour + " ч." + minute + " м.");
    }
}
